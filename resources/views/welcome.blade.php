@extends('layouts.app')

@section('content')

	@foreach($posts as $post)
		<div class="col-md-4 col-xs-12">
	        <div class="second-article">
                <div class="img-article">
                    <img src="{{ asset('storage/post/' . str_slug($post->title, '-') . '/img/' . $post->image) }}" alt="{{ $post->title }}" class="img-responsive img-center">
                </div>
	            <div class="article-body">
	                <div class="title">{{ $post->title }}</div>
	                <span>Autor: {{ $post->user->name }}</span>
	                <hr>
	            </div>
	        </div>
	    </div>
    @endforeach

@endsection