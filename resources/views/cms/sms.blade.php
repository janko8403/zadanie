@extends('cms.header')

@section('content')

	<div class="content-wrapper py-3">

        <div class="container-fluid">

            <!-- Breadcrumbs -->
            <ol class="breadcrumb">
              <li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Wyślij sms</li>
            </ol>

            <!-- Icon Cards -->
            <div class="row">

                <div class="col-md-12">

                <div class="card mb-3">
                    
                    <div class="card-body">

                    	@if (Session::has('send_sms'))
                            <div class="alert-message" role="alert">
                                <div class="row">
                                    <div class="col-md-2">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="message">
                                            {{Session::get('send_sms')}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <form class="form-horizontal" id="add" method="POST" action="{{ url('sms') }}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for ="phone" class="col-md-4 control-label">Podaj nr telefonu</label>

                                <div class="col-md-12">
                                    <input type="text" name="phone" class="form-control" id="phone" value="+48576396011">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for ="message" class="col-md-4 control-label">Treść wiadmości</label>

                                <div class="col-md-12">
                                    <input type="text" name="message" class="form-control" id="message">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary">
                                        Wyślij sms
                                    </button>
                                </div>
                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </div>
    
    </div>

@endsection