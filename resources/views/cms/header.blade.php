<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <!-- Custom fonts for this template -->
    <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Plugin CSS -->
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">

  </head>

<body class="fixed-nav" id="page-top">
    <div id="app">

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">

            <a class="navbar-brand" href="{{ url('cms/dashboard') }}">Cms Test</a>

            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav navbar-sidenav">

                    <li class="nav-item {{ (Request::is('cms/dashboard') ? 'active' : '') }}" data-toggle="tooltip" data-placement="right" title="Dashboard">
                        <a class="nav-link" href="{{url('cms/dashboard')}}">
                            <i class="fa fa-fw fa-dashboard"></i>
                            <span class="nav-link-text">Dashboard</span>
                        </a>
                    </li>

                    <li class="nav-item {{ (Request::is('cms/posts') ? 'active' : '') }}" data-toggle="tooltip" data-placement="right" title="Wpisy">
                        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseWpisy">
                            <i class="fa fa-folder-open" aria-hidden="true"></i>
                            <span class="nav-link-text">Wpisy</span>
                        </a>
                        
                        <ul class="sidenav-second-level collapse" id="collapseWpisy">
                            <li>
                                <a href="{{url('cms/posts')}}">Wszystkie wpisy</a>
                            </li>
                            <li>
                                <a href="{{url('cms/posts/create')}}">Dodaj nowy</a>
                            </li>
                        </ul>
                    </li>

                    @if ( Auth::check() && is_admin())
                        <li class="nav-item {{ (Request::is('cms/users') ? 'active' : '') }}" data-toggle="tooltip" data-placement="right" title="Użytkownicy">
                            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseUzytkownicy">
                                <i class="fa fa-users" aria-hidden="true"></i>
                                <span class="nav-link-text">Użytkownicy</span>
                            </a>
                            
                            <ul class="sidenav-second-level collapse" id="collapseUzytkownicy">
                                <li>
                                    <a href="{{url('cms/users')}}">Wszyscy użytkownicy</a>
                                </li>
                                <li>
                                    <a href="{{url('cms/users/create')}}">Dodaj nowego</a>
                                </li>
                            </ul>
                        </li>
                    @endif

                </ul>

                <ul class="navbar-nav sidenav-toggler">

                    <li class="nav-item">
                        <a class="nav-link text-center" id="sidenavToggler">
                            <i class="fa fa-fw fa-angle-left"></i>
                        </a>
                    </li>

                </ul>

                <ul class="navbar-nav ml-auto">

                    <li class="nav-item">
                        <a target="_blank" class="nav-link" href="{{ url('/') }}">
                            <i class="fa fa-tv" aria-hidden="true"></i>
                            Przejdź do strony
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                            Wyloguj się
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        
                    </li>
                    
                </ul>
            </div>
        </nav>

    @yield('content')

    <!-- Scroll to Top Button -->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/popper/popper.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    
    <!-- Plugin JavaScript -->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable( {
                "sorting": false,
                "lengthMenu": [ 25, 50 ],
                "language": {
                    "search": "Szukaj:"
                }
            } );
        } );
    </script>

    <script src="{{ asset('vendor/tinymce/tinymce.min.js') }}"></script>
    <script>
        tinymce.init({
          selector: 'textarea',
          height: 500,
          menubar: false,
          plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
          ],
          toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
          content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css']
        });
    </script>

    <script src="{{ asset('js/sb-admin.min.js') }}"></script>

    <script>
        $(".alert-message").fadeIn(200).delay(1500).fadeOut(1000);
    </script>

  </body>

</html>