@extends('cms.header')

@section('content')

    <div class="content-wrapper py-3">

        <div class="container-fluid">

            <!-- Breadcrumbs -->
            <ol class="breadcrumb">
              <li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Użytkownicy</li>
            </ol>

            <!-- Icon Cards -->
            <div class="row">

                <div class="col-md-12">

                <div class="card mb-3">
                    
                    <div class="card-body">

                        @if (Session::has('send_sms'))
                            <div class="alert-message" role="alert">
                                <div class="row">
                                    <div class="col-md-2">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="message">
                                            {{Session::get('send_sms')}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if (Session::has('send_message'))
                            <div class="alert-message" role="alert">
                                <div class="row">
                                    <div class="col-md-2">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="message">
                                            {{Session::get('send_message')}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if (Session::has('user_created'))
                            <div class="alert-message" role="alert">
                                <div class="row">
                                    <div class="col-md-2">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="message">
                                            {{Session::get('user_created')}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if (Session::has('edit_created'))
                            <div class="alert-message" role="alert">
                                <div class="row">
                                    <div class="col-md-2">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="message">
                                            {{Session::get('edit_created')}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if (Session::has('user_destroy'))
                            <div class="alert-message" role="alert">
                                <div class="row">
                                    <div class="col-md-2">
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="message">
                                            {{Session::get('user_destroy')}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="table-responsive">

                            @if ($users->count() === 1)
                                <div class="alert alert-danger" role="alert">
                                    Brak użytkowników
                                </div>
                            @else

                                <table class="table table-bordered" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>Nazwa</th>
                                            <th>E-mail</th>
                                            <th>Telefon</th>
                                            <th>Rola</th>
                                            <th>Akcja</th>
                                        </tr>
                                    </thead>

                                    <tfoot>
                                        <tr>
                                            <th>id</th>
                                            <th>Nazwa</th>
                                            <th>E-mail</th>
                                            <th>Telefon</th>
                                            <th>Rola</th>
                                            <th>Akcja</th>
                                        </tr>
                                    </tfoot>

                                    <tbody>

                                        @foreach ($users as $user)

                                            @if ($user->role_id == 1)

                                            @else

                                                <tr>
                                                    <td>{{ $user->id }}</td>
                                                    <td>{{ $user->name }}</td>
                                                    <td>{{ $user->email }}</td>  
                                                    <td>{{ $user->phone }}</td>  

                                                    <td>
                                                        @if ( $user->role_id == 1)
                                                            Super Admin
                                                        @endif
                                                        @if ( $user->role_id == 2)
                                                            User
                                                        @endif
                                                    </td>

                                                    @if ( Auth::check() && is_admin())
                                                        <td>
                                                            <div class="row">
                                                                <div class="col-xs-3 col-xs-offset-4">
                                                                    <a class="btn-action edit" href="{{ url('cms/users/' . $user->id . '/edit') }}">
                                                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                                                    </a> 
                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('cms/users/' . $user->id) }}">
                                                                        {{ csrf_field() }}
                                                                        <input type="hidden" name="_method" value="delete">
                                                        
                                                                        <button type="submit" class="btn-action delete">
                                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                                        </button>
                                                                    </form>
                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <a class="btn-action" href="{{ url('/send-mail/' . $user->email ) }}">
                                                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                                                    </a> 
                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <a class="btn-action" href="{{ url('/send-sms/'. $user->phone) }}">
                                                                        <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                                                    </a> 
                                                                </div>
                                                            </div>
                                                        </td>
                                                    @endif

                                                </tr>

                                            @endif
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                                
                            @endif

                        </div>
                    </div>

                    <div class="card-footer small text-muted">
                        Zapisani użytkownicy
                    </div>

                </div>

            </div>

        </div>
    
    </div>

@endsection
