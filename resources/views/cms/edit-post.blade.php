@extends('cms.header')

@section('content')

    <div class="content-wrapper py-3">

        <div class="container-fluid">

            <!-- Breadcrumbs -->
            <ol class="breadcrumb">
              <li class="breadcrumb-item active"><i class="fa fa-folder-open" aria-hidden="true"></i> Edytuj wpis</li>
            </ol>

            <!-- Icon Cards -->
            <div class="row">

                <div class="col-md-12">

                <div class="card mb-3">
                    
                    <div class="card-body">

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('cms/posts/' . $post->id) }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title" class="col-md-4 control-label">Nazwa wpisu</label>

                                <div class="col-md-12">
                                    <input id="title" type="text" class="form-control" name="title" value="{{ $post->title }}">

                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="text" class="col-md-4 control-label">Treść</label>

                                <div class="col-md-12">
                                    <textarea id="text" type="text" class="form-control" name="text">
                                        {{ $post->text }}
                                    </textarea>

                                </div>
                            </div>

                            @if ($post->image)
                                <div class="col-md-2 col-xs-6">
                                    <div class="block">
                                        <img class="img-fluid img-thumbnail" src="{{ asset('storage/post/' . str_slug($post->title, '-') . '/img' . $post->image) }}">
                                    </div>
                                </div>
                            @endif

                            <div class="form-group">
                                <label for ="image" class="col-md-4 control-label">Obrazek wyróżniający</label>

                                <div class="col-md-12">
                                    <input type="file" name="image" class="form-control" id="image">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary">
                                        Edytuj wpis
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>

                </div>

            </div>

        </div>
    
    </div>

@endsection
