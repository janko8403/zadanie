@extends('cms.header')

@section('content')

    <div class="content-wrapper py-3">

        <div class="container-fluid">

            <!-- Breadcrumbs -->
            <ol class="breadcrumb">
              <li class="breadcrumb-item active"><i class="fa fa-fw fa-dashboard"></i> Dashboard</li>
            </ol>

            <!-- Icon Cards -->
            <div class="row">

                <div class="col-xl-6 col-sm-6 mb-3">
                    <div class="card o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fa fa-file-text" aria-hidden="true"></i>
                            </div>
                            <div class="mr-5">
                                @if (is_admin())
                                    {{ $posts->count() }} Wpis(ów)
                                @else
                                    {{ $postsCount->count() }} Wpis(ów)
                                @endif
                            </div>
                        </div>
                        <a href="{{url('cms/posts')}}" class="card-footer clearfix small z-1">
                            <span class="float-left">Zobacz wszystkie</span>
                            <span class="float-right">
                                <i class="fa fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>

                @if ( Auth::check() && is_admin())
                    <div class="col-xl-6 col-sm-6 mb-3">
                        <div class="card o-hidden h-100">
                            <div class="card-body">
                                <div class="card-body-icon">
                                    <i class="fa fa-child" aria-hidden="true"></i>
                                </div>
                                <div class="mr-5">
                                    {{ $users->count() }} użytkownik(ów)!
                                </div>
                            </div>
                            <a href="{{url('cms/users')}}" class="card-footer clearfix small z-1">
                                <span class="float-left">Zobacz wszystkich</span>
                                <span class="float-right">
                                    <i class="fa fa-angle-right"></i>
                                </span>
                            </a>
                        </div>
                    </div>
                @endif

            </div>

        </div>

    </div>

@endsection
