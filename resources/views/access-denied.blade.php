@extends('layouts.app')

@section('content')
         
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 show">
                    <div class="card">
                        <div class="card-content">
                            <div class="empty-space"></div>
                            <h4 class="text-center">Aby moć zobaczyć treść tej strony, musisz się zalogować! <a href="{{ url('login') }}">Zaloguj się</a></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection