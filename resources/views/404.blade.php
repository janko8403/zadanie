<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 show">
                        <div class="card">
                            <div class="card-content">
                                <div class="empty-space"></div>
                                <h1 class="text-center">Strony nie odnaleziono</h1>
                                <a class="text-center" style="margin: 0 auto;display: block;" href="{{ url('/') }}">Wróć do strony głównej</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
    
