<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Welcome
Route::get('/', 'WelcomeController@index');

// Dashboard
Route::get('cms/dashboard', 'DashboardController@dashboard');

// Users
Route::resource('/cms/users', 'UsersController');

// Posts
Route::resource('/cms/posts', 'PostsController');

// Access denied
Route::get('/brak-dostepu', 'AccessDeniedController@access');

// Send Mail
Route::get('/send-mail/{email}', 'SendMailController@index');

// Send Sms
Route::get('/send-sms/{phone}', 'SendSmsController@index');

// GetAll Post Repositories
Route::resource('/get', 'PostRepositoryController');

Auth::routes();