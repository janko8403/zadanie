<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;

class WelcomeController extends Controller
{
    public function index()
    {
    	$posts = Post::orderBy('id', 'desc')->get();
    	return view('welcome', compact('posts'));
    }
}
