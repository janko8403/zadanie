<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AccessDeniedController extends Controller
{
    public function access()
    {
    	return view('access-denied');
    }
}
