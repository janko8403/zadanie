<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Nexmo\Laravel\Facade\Nexmo;
use Session;

class SendSmsController extends Controller
{
	public function __construct()
    {
        $this->middleware('admin_permission');
    }
    
    public function index($phone)
    {
    	Nexmo::message()->send([
		    'to'   => $phone,
		    'from' => '16105552344',
		    'text' => 'Cms test'
		]);

		Session::flash('send_sms', 'SmS wysłany poprawnie');
        return redirect('cms/users');
    }
}
