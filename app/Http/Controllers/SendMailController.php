<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Session;

class SendMailController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin_permission');
    }
    
    public function index($email)
    {
    	$data = array(
            'subject' => 'Mail testowy',
            'email' => $email
        );  

        Mail::send('mail', $data, function($message) use($data) {
            $message->to($data['email']);
            $message->from('from@example.com');
            $message->subject($data['subject']);
        });

        Session::flash('send_message', 'Mail wysłany poprawnie');
        return redirect('cms/users');
    }
}
