<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PostRepository;

class PostRepositoryController extends Controller
{
	private $post;

    public function __construct(PostRepository $post)
    {
    	$this->post = $post;
    }

    public function index()
    {
    	return $this->post->all();
    }

    public function show($id)
    {
    	return $this->post->find($id);
    }

    public function create(array $attributes)
    {
        return $this->post->create($attributes);
    }
    
    public function update($id, array $attributes)
    {
        $post = $this->post->findOrFail($id);
        $post->update($attributes);
        return $post;
    }
    
    public function delete($id)
    {
        $this->getById($id)->delete();
        return true;
    }

}
