<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Mailable;
use Nexmo\Laravel\Facade\Nexmo;
use Session;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin_permission');
    }

    public function index()
    {
        $users = User::orderBy('id', 'asc')->get();
        return view('cms/users', compact('users'));
    }

    public function create()
    {
        return view('cms/add-user');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required', 'email' => 'required|unique:users|email', 'phone' => 'required', 'password' => 'required|min:6', 'password_confirmation' => 'same:password'], [
            'required' => 'To pole jest wymagane', 'unique' => 'Taki e-mail juz istnieje w bazie', 'min' => 'Musisz podać minimalnie 6 znaków', 'same' => 'Hasło musi byc takie samo']
        );

        $name = $request['name'];
        $email = $request['email'];
        $phone = $request['phone'];
        $password = bcrypt($request['password']);
        $role_id = 2;
        
        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->phone = '48' . $phone;
        $user->password = $password;
        $user->role_id = $role_id;

        $user->save();

        Session::flash('user_created', 'Użytkownik dodany poprawnie');
        return redirect('cms/users');
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('cms/edit-user', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required', 'phone' => 'required'], [
            'required' => 'To pole jest wymagane']
        );

        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        if (!empty($request->password)) {
            $user->password = bcrypt($request->password);
        }

        $user->save();

        Session::flash('edit_created', 'Użytkownik zedytowany poprawnie');
        return redirect('cms/users');
    }
    
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        Session::flash('user_destroy', 'Użytkownik usunięty poprawnie');
        return redirect('cms/users');
    }
}
