<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Post;

class DashboardController extends Controller
{

	public function __construct()
    {
        $this->middleware('admin_user_permission');
    }

    public function dashboard()
    {
    	$posts = Post::orderBy('id', 'asc')->get();
        $postsCount = Post::where('user_id', auth()->user()->id)->get();
    	$users = User::orderBy('id', 'asc')->get();
        return view('cms/dashboard', compact('posts', 'users', 'postsCount'));
    }
}
