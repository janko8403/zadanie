<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Post;
use App\User;
use Session;

class PostsController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin_user_permission');
    }
    
    public function index()
    {
        $posts = Post::orderBy('id', 'desc')->get();
        $postsCount = Post::where('user_id', auth()->user()->id)->get();
        return view('cms/posts', compact('posts', 'postsCount'));
    }

    public function create()
    {
        return view('cms/new-post');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required'], [
            'required' => 'To pole jest wymagane']
        );

        $title = $request['title'];
        $user_id = $request['user_id'];
        $text = $request['text'];

        // dd($title);
       
        $post = new Post();
        $post->title = $title;
        $post->user_id = $user_id;
        $post->text = $text;

        if ($request->file('image')) {
            $post_image_path = 'public/post/' . str_slug($request['title'], '-') . '/img';
            $upload_path = $request->file('image')->store($post_image_path);
            $image_filename = str_replace($post_image_path . '', '', $upload_path);
            $post->image = $image_filename;
        }

        $post->save();

        Session::flash('post_created', 'Wpis dodany poprawnie');
        return redirect('cms/posts');
    }

    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('cms/edit-post', compact('post'));
    }
   
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required'], [
            'required' => 'To pole jest wymagane']
        );

        $post = Post::findOrFail($id);
        $post->title = $request->title;
        $post->text = $request->text;

        if ($request->file('image')) {
            $post_image_path = 'public/post/' . str_slug($request['title'], '-') . '/img';
            $upload_path = $request->file('image')->store($post_image_path);
            $image_filename = str_replace($post_image_path . '', '', $upload_path);
            $post->image = $image_filename;
        }

        $post->save();
        
        return back();
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();

        Session::flash('post_destroy', 'Wpis usunięty poprawnie');
        return redirect('cms/posts');
    }
}
