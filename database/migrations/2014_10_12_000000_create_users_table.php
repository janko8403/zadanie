<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone');
            $table->tinyInteger('role_id')->unsigned();
            $table->rememberToken();
            $table->timestamps();

        });
        
        DB::table('users')->insert(array('id' => '1', 'name' => 'Jan Kowalski', 'email' => 'test@test.pl', 'password' => bcrypt('111111'), 'role_id' => '1', 'phone' => '48576396011'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
